import random
import string
import unittest

import requests
from urllib3.connectionpool import xrange


class MyTestCase(unittest.TestCase):
    base_URI = "https://petstore.swagger.io/v2"
    pet_endpoint = "/pet"
    pet_id = ''.join([random.choice(string.digits) for n in xrange(10)])
    pet_name = "PetName_" + pet_id

    def test_1_check_status_of_pets(self):
        params = {'status': 'sold'}
        r = requests.get(self.base_URI + self.pet_endpoint + '/findByStatus', params=params)
        items = r.json()
        for item in items:
            self.assertEqual(item['status'], 'sold')

    def test_2_pet_create(self):
        model = {
            "id": self.pet_id,
            "category": {
                "id": 0,
                "name": "string"
            },
            "name": self.pet_name,
            "photoUrls": ['string'],
            "tags": [
                {
                    "id": 0,
                    "name": "string"}
            ],
            "status": "available"}

        headers = {'Accept': 'application/json', "Content-Type": 'application/json'}
        r = requests.post(self.base_URI + self.pet_endpoint, json=model, headers=headers)

        json_response = r.json()

        print(json_response)
        self.assertEqual(str(json_response['id']), self.pet_id)
        self.assertEqual(json_response['name'], self.pet_name)

    def test_3_update_pet_with_form_data(self):
        new_pet_name = "PetName_" + ''.join([random.choice(string.digits) for n in xrange(10)])
        headers = {"Content-Type": 'application/x-www-form-urlencoded'}
        params = {'name': new_pet_name, 'status': "sold"}
        r = requests.post(self.base_URI + self.pet_endpoint + "/" + self.pet_id, data=params, headers=headers)
        print(r.status_code)

        r = requests.get(self.base_URI + self.pet_endpoint + "/" + self.pet_id)
        json_response = r.json()
        self.assertEqual(json_response['name'], new_pet_name)
        self.assertEqual(json_response['status'], 'sold')

    def test_4_delete_pet(self):
        headers = {"api_key": "special-key"}
        r = requests.delete(self.base_URI + self.pet_endpoint + "/" + self.pet_id, headers=headers)
        self.assertEqual(r.status_code, 200)

    def test_5_check_is_pet_deleted(self):
        r = requests.get(self.base_URI + self.pet_endpoint + "/" + self.pet_id)
        json_response = r.json()
        self.assertEqual(r.status_code, 404)
        self.assertEqual(json_response['message'], "Pet not found")


if __name__ == '__main__':
    test_loader = unittest.TestLoader()
    suite = test_loader.loadTestsFromTestCase(MyTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
