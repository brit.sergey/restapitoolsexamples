﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using IO.Swagger.Model;
using System.Collections.Generic;
using System.Net;

namespace UnitTestProject1
{
	[TestClass]
	public class RestSharpTests
	{
		private static string API_URL = "https://petstore.swagger.io/v2";
		private static string PET_ENDPOINT = "/pet";
		static RestClient restClient = new RestClient(API_URL);
		static long id = GenerateTimeStamp();
		static string petName = "PetName_" + id;

		[TestMethod]
	    public void CreatePetTest()
		{
			var request = new RestRequest(PET_ENDPOINT, Method.POST);
			var json = "{\n" +
		   "  \"id\": \""+id+"\",\n" + "  \"category\": {\n" + "    \"id\": 0,\n" + "    \"name\": \"string\"\n" + "  },\n"
		   + "  \"name\": \""+petName+"\",\n" + "  \"photoUrls\": [\n" + "    \"string\"\n" + "  ],\n" + "  \"tags\": [\n" + "    {\n" + "      \"id\": 0,\n"
		   + "      \"name\": \"string\"\n" + "    }\n" + "  ],\n" + "  \"status\": \"available\"\n" + "}"; 
			request.AddJsonBody(json);
		//	request.AddHeader("Accept", "application/json");
		//	request.AddHeader("ContentType", "application/json");
			var response = restClient.Post<Pet>(request);

			Assert.AreEqual(response.Data.Id, id);
			Assert.AreEqual(response.Data.Name, petName);
			Assert.AreEqual(response.Data.Status, Pet.PetStatus.available);
    	}

		[TestMethod]
		public void GetPetsByStatusTest() {
			var request = new RestRequest(PET_ENDPOINT + "/findByStatus", Method.GET);
			request.AddQueryParameter("status", Pet.PetStatus.available.ToString());
			var response = restClient.Get<List<Pet>>(request).Data;

			foreach(Pet pet in response){
				Assert.AreEqual(pet.Status, Pet.PetStatus.available);
			}		
		}

		[TestMethod]
		public void UpdatePetWithFormData()
		{
			String petName1 = "PetName_" + GenerateTimeStamp();
			var request = new RestRequest(PET_ENDPOINT + "/{id}");
			request.AddParameter("id", id, ParameterType.UrlSegment);
			request.AddParameter("name", petName1, ParameterType.GetOrPost);
			request.AddParameter("status", Pet.PetStatus.sold, ParameterType.GetOrPost);
			restClient.Post(request);

			request = new RestRequest(PET_ENDPOINT + "/{id}");
			request.AddParameter("id", id, ParameterType.UrlSegment);
			var response = restClient.Get<Pet>(request);

			Assert.AreEqual(response.Data.Name, petName1);
			Assert.AreEqual(response.Data.Status, Pet.PetStatus.sold);
		}

		[TestMethod]
		public void DeletePet()
		{
			var request = new RestRequest(PET_ENDPOINT + "/{id}", Method.DELETE);
			request.AddParameter("id", id, ParameterType.UrlSegment);
			request.AddHeader("api_key", "special-key");
			var response = restClient.Delete(request);
			Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
		}

		[TestMethod]
		public void CheckPetIsDeleted()
		{
			var request = new RestRequest(PET_ENDPOINT + "/{id}", Method.GET).AddParameter("id", id, ParameterType.UrlSegment);
			var response = restClient.Get<ApiResponse>(request);
			Assert.AreEqual(response.StatusCode, HttpStatusCode.NotFound);
			Assert.AreEqual(response.Data.Message, "Pet not found");
		}
		private static long GenerateTimeStamp()
		{
			return DateTime.UtcNow.Ticks;
		}
	}
	
}
