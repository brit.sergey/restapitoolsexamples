package org.brit.tests.ra;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.brit.tests.models.Pet;
import org.brit.tests.models.PetStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

/**
 * @author sbrit
 */
public class RestAssuresTests {
    long id = generateTimeStamp();
    private String petName = "PetName_" + id;
    private String petJson = "{\n" +
            "  \"id\": " + "%d,\n" + "  \"category\": {\n" + "    \"id\": 0,\n" + "    \"name\": \"string\"\n" + "  },\n"
            + "  \"name\": \"%s\",\n" + "  \"photoUrls\": [\n" + "    \"string\"\n" + "  ],\n" + "  \"tags\": [\n" + "    {\n" + "      \"id\": 0,\n"
            + "      \"name\": \"string\"\n" + "    }\n" + "  ],\n" + "  \"status\": \"available\"\n" + "}";
    private String PET_ENDPOINT = "/pet";

    @BeforeSuite
    public void beforeSuite() {
        RestAssured.baseURI = "https://petstore.swagger.io/";
        RestAssured.basePath = "/v2";
        RestAssured.requestSpecification = new RequestSpecBuilder().setAccept(ContentType.JSON).setContentType(ContentType.JSON).build();
    }

    @Test
    public void getPetsByStatus() {
        Response response =
                given()
                        .log().all()
                        .param("status", PetStatus.available)
                        .when()
                        .get(PET_ENDPOINT + "/findByStatus")
                        .prettyPeek();

        List<Pet> pets = response.jsonPath().getList("", Pet.class);
        response
                .then().assertThat()
                .body("status", hasItem(PetStatus.available.name()));
        pets.forEach(pet -> Assert.assertEquals(pet.getStatus(), PetStatus.available));
    }

    @Test
    public void createPet() {
        given()
                .body(String.format(petJson, id, petName))
                .post(PET_ENDPOINT)
                .then().assertThat()
                .body("id", equalTo(id), "name", equalTo(petName));
    }

    @Test(dependsOnMethods = "createPet")
    public void updatePetWithFormData() {
        String petName = "PetName_" + generateTimeStamp();
        given()
                .contentType(ContentType.URLENC)
                .formParam("name", petName)
                .formParam("status", PetStatus.sold.name())
                .pathParam("id", this.id)
                .post(PET_ENDPOINT + "/{id}");

        given()
                .pathParam("id", this.id)
                .get(PET_ENDPOINT + "/{id}")
                .then().assertThat()
                .body("name", equalTo(petName), "status", equalTo(PetStatus.sold.name()));
    }

    @Test(dependsOnMethods = "updatePetWithFormData")
    public void deletePet() {
        given()
                .header("api_key", "special-key")
                .pathParam("id", this.id)
                .delete(PET_ENDPOINT + "/{id}")
                .then().assertThat().statusCode(200);
    }

    @Test(dependsOnMethods = "deletePet")
    public void checkPetIsDeleted() {
        given()
                .pathParam("id", this.id)
                .get(PET_ENDPOINT + "/{id}")
                .then()
                .statusCode(404)
                .body("message", equalTo("Pet not found"));
    }

    private long generateTimeStamp() {
        return new Date().getTime();
    }
}
