package org.brit.tests.okhttp;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;
import okhttp3.*;
import org.brit.tests.models.ModelApiResponse;
import org.brit.tests.models.Pet;
import org.brit.tests.models.PetStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

public class OkHTTPTests {
  @Test
  public void getPetsByStatus() throws IOException {
    HttpUrl httpUrl = builder.addPathSegment("findByStatus").addQueryParameter("status", PetStatus.available.name()).build();
    Request request = new Request.Builder().addHeader("Accept", "application/json").url(httpUrl).get().build();
    Response response = client.newCall(request).execute();

    Type type = Types.newParameterizedType(List.class, Pet.class);
    JsonAdapter<List<Pet>> adapter = moshi.adapter(type);
    List<Pet> pets = adapter.fromJson(response.body().string());

    pets.forEach(pet -> Assert.assertEquals(pet.getStatus(), PetStatus.available));
    builder.removePathSegment(2).removeAllQueryParameters("status");
  }

  @Test
  public void createPet() throws IOException {
    HttpUrl httpUrl = builder.build();
    RequestBody requestBody = RequestBody
            .create(MediaType.parse("application/json"), String.format(petJson, id, petName));
    Request request = new Request
            .Builder()
            .addHeader("Accept", "application/json")
            .url(httpUrl)
            .post(requestBody).build();
    Response response = client.newCall(request).execute();
    Pet pet = moshi.adapter(Pet.class).fromJson(response.body().string());
    Assert.assertEquals(pet.getId(), (Long) id);
    Assert.assertEquals(pet.getName(), petName);
  }

  @Test(dependsOnMethods = "createPet")
  public void updatePetWithFormData() throws IOException {
    String petName = "PetName_" + generateTimeStamp();
    HttpUrl httpUrl = builder.addPathSegment(Long.toString(this.id)).build();
    RequestBody requestBody = new FormBody.Builder().add("name", petName).add("status", PetStatus.sold.name()).build();
    Request request = new Request.Builder().url(httpUrl).post(requestBody).build();
    Response response = client.newCall(request).execute();

    request = new Request.Builder().url(httpUrl).get().build();
    response = client.newCall(request).execute();
    Assert.assertEquals(response.code(), 200);
    Pet pet = moshi.adapter(Pet.class).fromJson(response.body().string());
    Assert.assertEquals(pet.getStatus(), PetStatus.sold);
    Assert.assertEquals(pet.getName(), petName);
    builder.removePathSegment(2);
  }

  @Test(dependsOnMethods = "updatePetWithFormData")
  public void deletePet() throws IOException {
    HttpUrl httpUrl = builder.addPathSegment("" + id).build();
    Request request = new Request.Builder().url(httpUrl).addHeader("api_key", "special-key").delete().build();
    Response response = client.newCall(request).execute();
    Assert.assertEquals(response.code(), 200);
    builder.removePathSegment(2);
  }

  @Test(dependsOnMethods = "deletePet")
  public void checkPetIsDeleted() throws IOException {
    HttpUrl httpUrl = builder.addPathSegment("" + id).build();
    Request request = new Request.Builder().url(httpUrl).get().build();
    Response response = client.newCall(request).execute();
    Assert.assertEquals(response.code(), 404);
    ModelApiResponse modelApiResponse = moshi.adapter(ModelApiResponse.class).fromJson(response.body().string());
    Assert.assertEquals(modelApiResponse.getMessage(), "Pet not found");
  }

  private long generateTimeStamp() {
    return new Date().getTime();
  }

  private String petJson =
      "{\n" + "  \"id\": " + "%d,\n" + "  \"category\": {\n" + "    \"id\": 0,\n" + "    \"name\": \"string\"\n" + "  },\n" + "  \"name\": \"%s\",\n"
          + "  \"photoUrls\": [\n" + "    \"string\"\n" + "  ],\n" + "  \"tags\": [\n" + "    {\n" + "      \"id\": 0,\n"
          + "      \"name\": \"string\"\n" + "    }\n" + "  ],\n" + "  \"status\": \"available\"\n" + "}";

  private final Moshi moshi = new Moshi.Builder().build();
  HttpUrl.Builder builder = new HttpUrl.Builder().scheme("https").host("petstore.swagger.io").addPathSegments("v2/pet");
  OkHttpClient client = new OkHttpClient();
  long id = generateTimeStamp();
  private String petName = "PetName_" + id;

}
