package org.brit.tests.models;

/**
 * @author sbrit
 */
public enum PetStatus {
  available,
  sold,
  pending
}
