# RestApiToolsExamples

There are examples of RestAPI testing tools.

# Content:
* [Postman](https://www.getpostman.com/) - just read how to import test suite end environment to Postman
* **restapijava** directory contains 2 tools:
  *   [Rest-Assured](http://rest-assured.io/)
  *   [OkHttp](https://square.github.io/okhttp/)
* **pythonrest** is about  [Requests](https://2.python-requests.org/en/master/) python library
* **restsharp** is about [RestSharp](http://restsharp.org/) C# library 